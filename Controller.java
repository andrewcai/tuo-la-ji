import java.util.ArrayList;
import java.util.Collections;
import java.net.Socket;
import java.io.*;

public class Controller {
	
	// Players that one even indices will be one team, while odd is another.
	private ArrayList<Player> players;
	private ArrayList<ConnectionHandler> cList;
	int defendingTeam = 0;
	int host = -1;
	private boolean isFirstGame = true;
	private int pointsAcquired = 0;
	private Card trumpCard;
	
	public Controller(ArrayList<Player> playerList, ArrayList<ConnectionHandler> ch) {
		players = playerList;
		cList = ch;
	}

	
	/** 
	 * Play one round of Tuo La Ji. 
	 * 
	 * @return 0 if attacking team overturned, a positive number equal to
	 * how much the defending team's rank went up, and negative number for
	 * if the attacking team's rank went up.
	 */	
	public int playRound(int rank) {
		Deck deck = new Deck(players.size() / 2);
		//Randomly decides who draws first
		int turnDraw = (int) (Math.random()*players.size());
		// Declared rank cards go here
		ArrayList<Card> declaration = new ArrayList<Card>(3);
		// Holder for how the rank changes
		int rankChange = 0;			
		// Holds cards played during turn
		ArrayList<Card> choosen = new ArrayList<Card>(4);
		// Points played in turn
		int pointsInTurn = 0;
		// Temporary Hand is so String conversion is easier to format
		Hand temp = new Hand(rank);
		
		/* The next two objects are used to determine the highest card played in the turn, 
		 * additionally, this is updates the startingTurn value whenever a new value is found
		 * */
		CardComparator cc = new CardComparator(Card.JOKER, rank);
		Card highestCard = null;
		
		// For now assume typical setup, with 1 deck per 2 persons
		int numBottomCards;
		if (players.size() == 4) { 
			numBottomCards = 8;
		}
		else { 
			numBottomCards = players.size();
		}
		
		try 
		{
			for (int i = 0; i <players.size(); i++) {
				cList.get(i).out.writeUTF("Rank " + rank + "\nPhase 1\n");				
			}
			while (deck.cardsLeft() > numBottomCards) 
			{
				Card drawn = deck.draw();
				Hand h = players.get(turnDraw).hand;
				h.add(drawn);
				h.sort();
				cList.get(turnDraw).out.writeUTF("Drawn: " + drawn.toString() + "\n" + h.toString() + "\n");
				cList.get(turnDraw).out.writeUTF("go");
				String returnedValue = cList.get(turnDraw).in.readUTF ();
				if (returnedValue == null || returnedValue.trim().equals("")) { }
				else {
					Card declaredCard = Card.parseCard(returnedValue);
					int incidence = players.get(turnDraw).hand.count(returnedValue);
					if (!(declaredCard.getNumber() == rank || declaredCard.getSuit().equals(Card.JOKER)) || incidence <= declaration.size()) {
						cList.get(turnDraw).out.writeUTF("Failed!\n");
					} else if (players.get(turnDraw).hand.count(returnedValue) > declaration.size()) {
						if (isFirstGame) {
							host = turnDraw;
							isFirstGame = false;
						}
						int n = players.get(turnDraw).hand.count(returnedValue);
						declaration.clear();
						while (n > 0) {
							declaration.add(Card.parseCard(returnedValue));
							n--;
						}
						
						// Alert all other players of declaration
						for (int j = 0; j < players.size(); j++) {
							if (j == turnDraw) { continue; }
							cList.get(j).out.writeUTF("* * * * * * * * * * * * *\n" +
											players.get(turnDraw).name + " declared	" + declaration.toString() + "\n" +
													"* * * * * * * * * * * * *\n");
						}
					}
				}
				
				turnDraw = (turnDraw + 1)% players.size();
			}
			// Inform players drawing is over and the trump suit
			Card trumpCard = declaration.get(0);
			for (int j = 0; j < players.size(); j++) {
				String resp = "";
				if (trumpCard.getSuit().equals(Card.SPADES)) { 
					resp += "\nTrump suit is SPADES\n\nPhase 2\n";
					players.get(j).hand.trumpSuit = trumpCard.getSuit();
					cc.trumpSuit = Card.SPADES;
				}
				if (trumpCard.getSuit().equals(Card.HEARTS)) { 
					resp += "\nTrump suit is HEARTS\n\nPhase 2\n";
					players.get(j).hand.trumpSuit = trumpCard.getSuit();
					cc.trumpSuit = Card.HEARTS;
				}
				if (trumpCard.getSuit().equals(Card.CLUBS)) {
					resp += "\nTrump suit is CLUBS\n\nPhase\n";
					players.get(j).hand.trumpSuit = trumpCard.getSuit();
					cc.trumpSuit = Card.CLUBS;
				}
				if (trumpCard.getSuit().equals(Card.DIAMONDS)) { 
					resp += "\nTrump suit is DIAMONDS\n\nPhase 2\n";
					players.get(j).hand.trumpSuit = trumpCard.getSuit();
					cc.trumpSuit = Card.DIAMONDS;
				}
				if (trumpCard.equals(Card.JOKER)) { 
					resp += "\nTrump suit is NONE\n\nPhase 2\n";
					players.get(j).hand.trumpSuit = trumpCard.getSuit();
					cc.trumpSuit = Card.JOKER;
				}
				
				if (host == j) {
					Hand bottomCards = new Hand(); 
					while (deck.cardsLeft() > 0) {
						Card btm = deck.draw();
						players.get(j).hand.add(btm);
						bottomCards.add(btm);	
					}
					players.get(j).hand.sort();
					resp += "You are host! Added: " + bottomCards.toString() + "\n" 
							+ players.get(j).hand.toString() + "\n" 
							+ "Choose " + numBottomCards + " to get rid of: ";
					
					cList.get(j).out.writeUTF(resp);
					cList.get(j).out.writeUTF("go");
				} else {
					cList.get(j).out.writeUTF(resp + "Waiting for " + players.get(host).name 
							+ " to place bottom cards..");
				}
			}
			
			// Receive the bottom cards
			String bottomCards = cList.get(host).in.readUTF();
			
			String segment = bottomCards.substring(0,3); 
			String getCard = segment.replaceFirst("(10|\\w)(\\D)(\\s|$)", "$1$2");
			while (bottomCards.length() > 1) {
				players.get(host).bottom.add(Card.parseCard(getCard));
				bottomCards = bottomCards.substring(1);
				bottomCards = bottomCards.replaceFirst("(0\\D|\\D)(\\s)(.*$)", "$3");
				segment = bottomCards.substring(0,Math.min(3, bottomCards.length())); 
				getCard = segment.replaceFirst("(\\s?)(\\w)(\\w)(\\D?)(\\s*|$)", "$2$3$4");
			}
			
			for (ConnectionHandler ch : cList) {
				ch.out.writeUTF("\n\nPhase 3");
			}
			
			int startingTurn = host;
			int turnPlay = host;
			
			// Start card sequence
			while (!players.get(turnPlay).hand.getCards().isEmpty()) {
				// Message all players of current score
				for (int u = 0; u < players.size(); u++) {
					String announcement = "\n";
					if (u % 2 == 0) {
						announcement += "DEF | ";
					} else {
						announcement += "ATT | ";
					}
					players.get(u).hand.sort();
					cList.get(u).out.writeUTF(announcement + convertToRank(trumpCard) + " " + convertToSuit(trumpCard) + " | " + String.valueOf(pointsAcquired) + " pts. Captured by Attacking\n"
							+ "Choose cards to play: " + players.get(u).hand.toString() + "\n");
				}
				// Iterate so every player plays a card
				for (int c = 0; c < players.size(); c++, turnPlay = (turnPlay + 1)% players.size()) {
					cList.get(turnPlay).out.writeUTF("You: ");
					cList.get(turnPlay).out.writeUTF("go");
					String resp = cList.get(turnPlay).in.readUTF();
					choosen = parseMultipleCards(resp);
					// If the cards played are the highest in the turn so far
					if (highestCard == null || cc.compare(highestCard, choosen.get(0)) > 0) {
						highestCard = choosen.get(0);
						startingTurn = turnPlay;
					}
					
					// If there are points in the cards played
					for (Card pointsMaybe : choosen) {
						if (pointsMaybe.getNumber() == Card.FIVE) {
							pointsInTurn += 5;
						} else if (pointsMaybe.getNumber() == Card.TEN) {
							pointsInTurn += 10;
						} else if (pointsMaybe.getNumber() == Card.KING) {
							pointsInTurn += 10;
						}
					}
					
					temp.clear();
					temp.add(choosen);
					for (Card q : choosen) {
						players.get(turnPlay).hand.remove(q);
					}
					
					// Inform other players of choice
					for (int t = 0; t < players.size(); t++) {
						if (t != turnPlay) {
							cList.get(t).out.writeUTF(players.get(turnPlay).name + ": " + temp.toString());
						}
					}
				}
				
				turnPlay = startingTurn;
				if (startingTurn % 2 != host % 2) {
					pointsAcquired += pointsInTurn;
					pointsInTurn = 0;
				}	
		}
		
		int additionalPointsFromBottom = 0;
		int multiplier = 1;
		if (startingTurn % 2 != host % 2) {
			multiplier = 2;
		}
		if (Hand.isPair(choosen)) {
			multiplier = 4;
		}
		if (Hand.isTractor(choosen)) {
			multiplier = 8;
		}

		if (startingTurn % 2 != host % 2) {
			for (Card c : players.get(host).bottom.getCards()) {
				if (c.getNumber() == Card.FIVE) {
					if (players.get(host).bottom.getCards().contains(c)) {
						additionalPointsFromBottom += (multiplier*2)*5 - multiplier*5;
					} else {
						additionalPointsFromBottom += multiplier*5;
					}
				} else if (c.getNumber() == Card.TEN && players.get(host).bottom.getCards().contains(c)) {
					additionalPointsFromBottom += (multiplier*2)*10 - multiplier*10; 
				} else if (c.getNumber() == Card.KING && players.get(host).bottom.getCards().contains(c)) {
					additionalPointsFromBottom += (multiplier*2)*10 - multiplier*10; 
				}
			}
		}
		
		pointsAcquired += additionalPointsFromBottom;
		
		if (pointsAcquired == 0) {
			rankChange = 3;
		}
		else if (pointsAcquired < 40) { 
			rankChange = 2;
		}
		else if (pointsAcquired < 80) { 
			rankChange = 1;
		}
		else {
			rankChange = (80-pointsAcquired)/40;
		}
		
		for (ConnectionHandler ch : cList) {
			if (rankChange > 0) {
				ch.out.writeUTF("DEFENDING WINS! FINAL SCORE: " + pointsAcquired);
			} else {
				ch.out.writeUTF("ATTACKING WINS! FINAL SCORE: " + pointsAcquired);
			}
		}
		
			
		} catch (IOException e) {
			System.out.println("Error in Controller");
			e.printStackTrace();
		}

		return rankChange;
	}
	
	/**
	 * Plays one full game, until a team passes Ace rank.
	 */
	public void playGame() {
		int rankOfTeam1 = 2; // Team1 is the team that was host on the very FIRST round
		int rankOfTeam2 = 2;
		boolean currentDef = true; // True is Team1 false otherwise
		
		int value = playRound(2); 
		if (value > 0) {
			rankOfTeam1 += value;
			host = (host+2) % players.size();
		} else {
			rankOfTeam2 += -1 * value;
			currentDef = !currentDef;
			host = (host+1) % players.size();
		}
		while (rankOfTeam1 <= 14 || rankOfTeam2 <= 14) {
			if (currentDef) {
				value = playRound(rankOfTeam1);
				if (value > 0) {
					rankOfTeam1 += value;
					host = (host+2) % players.size();
				} else {
					rankOfTeam2 += -1 * value;
					currentDef = !currentDef;
					host = (host+1) % players.size();
				}
			} else {
				value = playRound(rankOfTeam2);
				if (value > 0) {
					rankOfTeam2 += value;
					host = (host+2) % players.size();
				} else {
					rankOfTeam1 += -1 * value;
					currentDef = !currentDef;
					host = (host+1) % players.size();
				}
			}

		}
		// Notifies all players that round is over
		for (ConnectionHandler ch : cList) { 
			try {
			ch.out.writeUTF("end");
			} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
		}
	}
	
	private String convertToSuit(Card c) {
		if (c.getSuit().equals(Card.SPADES))
			return "SPADES";
		else if (c.getSuit().equals(Card.HEARTS))
			return "HEARTS";
		else if (c.getSuit().equals(Card.CLUBS))
			return "CLUBS";
		else if (c.getSuit().equals(Card.DIAMONDS))
			return "DIAMONDS";
		else 
			return "JOKER";
	}
	
	private String convertToRank(Card c) {
		switch (c.getNumber()) {
		case 0:
			return "NO";
		case 1:
			return "NO";
		case 2:
			return "TWO";
		case 3:
			return "THREE";
		case 4:
			return "FOUR";
		case 5:
			return "FIVE";
		case 6:
			return "SIX";
		case 7:
			return "SEVEN";
		case 8:
			return "EIGHT";
		case 9:
			return "NINE";
		case 10:
			return "TEN";
		case 11:
			return "JACK";
		case 12:
			return "QUEEN";
		case 13:
			return "KING";
		case 14:
			return "ACE";
		}
		return "NONE";
	}
	
	private ArrayList<Card> parseMultipleCards(String str) {
		ArrayList<Card> list = new ArrayList<Card>(str.length() / 2);

		while (str.length() > 1) {
			String segment = str.substring(0,Math.min(3, str.length())); 
			String getCard = segment.replaceFirst("(\\s?)(10\\D|\\w\\D)(\\s*|$)", "$2");
			list.add(Card.parseCard(getCard));
			str = str.substring(1);
			str = str.replaceFirst("(0\\D|\\D)(\\s)(.*$)", "$3");
		}
		return list;
	}
}