import java.util.ArrayList;

public class Player {
	
	String name;
	public boolean isHost;
	public Hand hand;
	public Hand bottom;
	public Controller controller;
	
	public Player(String name) {
		this.name = name;
		isHost = false;
		hand = new Hand();
		bottom = new Hand();
	}

	public void play(ArrayList<Card> multiple) {
		for (Card c : multiple)
			hand.remove(c);
	}
	
	public void play(Card single) {
		hand.remove(single);
	}
	
}