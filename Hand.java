// Object representing a hand of Cards
import java.util.ArrayList;

import java.util.Collections;

public class Hand {

	private ArrayList<Card> cards;
	public String trumpSuit = Card.JOKER;
	public int rank = 2;
	
	public Hand() { 
		cards = new ArrayList<Card>();
	}
	
	public Hand(int rank) {
		cards = new ArrayList<Card>();
		this.rank = rank;
	}
	
	public void sort() {
		Collections.sort(cards, new CardComparator(trumpSuit, rank));
	}
	
	public ArrayList<Card> getCards() {
		return cards;
	}
	
	public void add(Card c) {
		cards.add(c);
	}
	
	public void add(ArrayList<Card> cl) {
		for (Card c : cl) 
			add(c);
	}
	
	public void setRank(int r) {
		setRank(Card.JOKER, r);
	}
	
	public void setRank(String trump, int r) {
		trumpSuit = trump;
		rank = r;
	}
		
	public Card find(String s) {
		for (Card c : cards) {
			if (c.toString().equals(s))
				return c;
		}
		return null;
	}
	
	public static boolean isPair(ArrayList<Card> e) {
		for (int i = 0; i < e.size()-1; i++) {
			if (e.get(i).equals(e.get(i+1))) {
				return true;
			} 
		}
		return false;
	}
	public static boolean isTractor(ArrayList<Card> e) {
		boolean isTractor = false;
		for (int i = 0; i < e.size(); i+=2) {
			if (e.get(i).equals(e.get(i+1))) {
				isTractor = true;
			} else {
				isTractor = false;
				return isTractor;
			}
		}
		return isTractor;
	}
	
	public int count(String s) {
		int counter = 0;
		for (Card c : cards) {
			if (c.toString().equals(s))
				counter++;
		}
		return counter;
	}
	
	public void remove(String s) {
		for (int i = 0; i < cards.size(); i++) {
			if (cards.get(i).toString().equals(s))
				cards.remove(i);
				return;
		}
	}
	
	public void remove(Card target) {
		for (int i = 0; i < cards.size(); i++) { 
			if (target.getSuit().equals(cards.get(i).getSuit()) && target.getNumber() == cards.get(i).getNumber()) {
				cards.remove(i);
				return;
			}
		}
	}

	public void clear() {
		cards.clear();
	}
	
	public String toString() {
		String x = "";
		for (Card c : cards)
			x += c.toString() + " ";
		
		x = x.substring(0, x.length()-1);
		return x;
	}
}