import java.net.*;
import java.io.*;
import java.util.Scanner;

public class Client {

	
	public static void main(String[] args) {
	
		System.out.print("What is your name? ");
		Scanner scan = new Scanner(System.in);
		String name = scan.nextLine();
		System.out.println();
		String serverName = args[0];
		int port = Integer.parseInt(args[1]);
      	try {
		    System.out.println("Connecting to " + serverName + " on port " + port);
            Socket client = new Socket(serverName, port);
	        System.out.println("Just connected to " + client.getRemoteSocketAddress());
		    OutputStream outToServer = client.getOutputStream();
			DataOutputStream out = new DataOutputStream(outToServer);
	        out.writeUTF(name);
			InputStream inFromServer = client.getInputStream();
			DataInputStream in = new DataInputStream(inFromServer);
			if(!in.readUTF().equals("pong")) {
				System.out.println("Failed to connect.");
				return;		
			}
			
			String output = in.readUTF();
			while (!output.equals("end")) {
				if (output.equals("go")) {
					while (!scan.hasNextLine());
					String input = scan.nextLine();
					out.writeUTF(input);
				} else {
					System.out.print(output);
				}
				output = in.readUTF();
			} 
			
			
			System.out.print("How many players? ");
			scan = new Scanner(System.in);
			int n = scan.nextInt();
			out.writeUTF(Integer.toString(n));
			System.out.println("Starting Tuo La Ji...");
			String team = in.readUTF();
			System.out.println(team);

			// Starting controller here
			String startMsg = in.readUTF();
			System.out.println(startMsg);
			String outputString = in.readUTF();
			while (!outputString.equals("end")) {
				System.out.print(outputString);
				if (scan.hasNextLine()) {
				String next = scan.nextLine();
				out.writeUTF(next);
				outputString = in.readUTF();
				}
			}
			
			String response = in.readUTF();
			System.out.print(response);
			
			// Close connection and end
			client.close();
		} catch (IOException e) {
			System.out.println("Error in connection.");
			e.printStackTrace();
		}
	}
}