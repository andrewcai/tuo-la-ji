import java.util.Comparator;

public class CardComparator implements Comparator<Card> {

	public String trumpSuit;
	public int rank;
	
	public CardComparator(String t, int rank) {
		trumpSuit = t;
		this.rank = rank;
	}
	
	@Override
	public int compare(Card first, Card second) {
		int suitFirst = convert(first.getSuit());
		int suitSecond = convert(second.getSuit());
		
		// Handle first if cards are equal
		if (suitFirst == suitSecond && first.getNumber() == second.getNumber()) {
			return 0; 
		}
		// If one of the cards is a JOKER
		else if (suitFirst == 0 || suitSecond == 0) {
			if (suitFirst == suitSecond) {
				if (first.getNumber() > second.getNumber()) {
					return -1;
				} else {
					return 1;
				}
			} else {
				if (suitFirst < suitSecond) {
					return -1;
				} else {
					return 1;
				}
			}
		}
		// If one of the cards is a rank card
		else if (first.getNumber() == rank || second.getNumber() == rank) {
			if (first.getNumber() == rank && second.getNumber() == rank) {
				if (suitFirst < suitSecond) {
					return -1;
				} else {
					return 1;
				}
			}
			else if (first.getNumber() == rank && second.getSuit().equals(Card.JOKER)) {
				return 1;
			}
			else if (first.getNumber() == rank) {
				return -1;
			}
			else if (second.getNumber() == rank && first.getSuit().equals(Card.JOKER)) {
				return -1;
			}
			else if (second.getNumber() == rank) {
				return 1;
			}
		}
		// Then handle if suits are unequal
		else if (suitFirst != suitSecond) {
			if (suitFirst < suitSecond) {
				return -1;
			} else {
				return 1;
			}
		} 
		// Same suit
		else {
			if (first.getNumber() > second.getNumber()) {
				return -1;
			} else {
				return 1;
			}
		}
		return 0;
	}
	
	/**
	 * Converts a String suit into a number. The lower the number, 
	 * the higher (relatively) the suit is. Uses alternating colors.
	 * 
	 * @param suit A string representing a suit
	 * @return A positive number representing the suit based on the trump suit
	 *  and alternating color ordering
	 */
	public int convert(String suit) {
		if (trumpSuit.equals(Card.SPADES)) {
			if (suit.equals(Card.SPADES)) {
				return 1;
			} else if (suit.equals(Card.HEARTS)) {
				return 2;
			} else if (suit.equals(Card.CLUBS)) {
				return 3;
			} else if (suit.equals(Card.DIAMONDS)) {
				return 4;
			} else {
				return 0;
			}
		} else if (trumpSuit.equals(Card.HEARTS)) {
			if (suit.equals(Card.SPADES)) {
				return 2;
			} else if (suit.equals(Card.HEARTS)) {
				return 1;
			} else if (suit.equals(Card.CLUBS)) {
				return 4;
			} else if (suit.equals(Card.DIAMONDS)) {
				return 3;
			} else {
				return 0;
			}
		} else if (trumpSuit.equals(Card.CLUBS)) {
			if (suit.equals(Card.SPADES)) {
				return 3;
			} else if (suit.equals(Card.HEARTS)) {
				return 2;
			} else if (suit.equals(Card.CLUBS)) {
				return 1;
			} else if (suit.equals(Card.DIAMONDS)) {
				return 4;
			} else {
				return 0;
			}
		} else if (trumpSuit.equals(Card.DIAMONDS)) {
			if (suit.equals(Card.SPADES)) {
				return 2;
			} else if (suit.equals(Card.HEARTS)) {
				return 3;
			} else if (suit.equals(Card.CLUBS)) {
				return 4;
			} else if (suit.equals(Card.DIAMONDS)) {
				return 1;
			} else {
				return 0;
			}
		} else {
			if (suit.equals(Card.SPADES)) {
				return 1;
			} else if (suit.equals(Card.HEARTS)) {
				return 2;
			} else if (suit.equals(Card.CLUBS)) {
				return 3;
			} else if (suit.equals(Card.DIAMONDS)) {
				return 4;
			} else {
				return 0;
			}
		}  
	}
}
