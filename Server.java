/* Server handles all game requests and manages all aspects of the game */
import java.net.*;
import java.io.*;
import java.util.*;

public class Server
{
   private ServerSocket serverSocket;
   private ArrayList<Player> playerList;
   private ArrayList<ConnectionHandler> ch;

   public Server() throws IOException {
		this(4444); // Random port, if none specified
   }
   public Server(int port) throws IOException {
      	serverSocket = new ServerSocket(port);
		ch = new ArrayList<ConnectionHandler>(6);
		playerList = new ArrayList<Player>(6);
   }

   public void run() {
	  int numPlayers = -1; // Number of players for a game
	  int numConnected = 0;
	  
   	  // Wait until all clients wishing to play are connected
   	  while(numPlayers != numConnected) 
   	  {
   		  try 
   		  {
   			  System.out.println("Waiting for client on port " + serverSocket.getLocalPort() + "...");
   			  Socket server = serverSocket.accept();
    		  System.out.println("Just connected to " + server.getRemoteSocketAddress());
    		  Runnable connectionHandler = new ConnectionHandler(server);
    		  ConnectionHandler yz = (ConnectionHandler) connectionHandler;
    		  new Thread(connectionHandler).start();
    		  int position = (int)(Math.random()*(playerList.size()+1));
    		  playerList.add(position, new Player(yz.in.readUTF()));		
    		  ch.add(position, yz);    		  
    		  yz.out.writeUTF("pong");
    		  yz.out.writeUTF("How many players? ");
    		  yz.out.writeUTF("go");
			  numPlayers = Integer.parseInt(yz.in.readUTF());
			  System.out.println("Client selected " + numPlayers);
			  yz.out.writeUTF("Waiting for other players...\n\n");
			  numConnected++;
    	  } catch (IOException e) { e.printStackTrace(); }
   	  } // At this point, all players have joined the game
	
	System.out.println("All players have entered the game!");
	
	// Sending of Team Names
	for (int i = 0; i < playerList.size(); i++) {
		try {
			if (i % 2 == 0) {
				String teammates = "";
				for(int j = 0; j < playerList.size(); j+=2) { 
					if (j != i) {
						teammates+= " " + playerList.get(j).name;
					}
				}
				ch.get(i).out.writeUTF("Your Team: Honey Badgers Gone BEAST Mode\nTeammates:" + teammates + "\n\n");
			} else { 
				String teammates = "";
				for(int j = 1; j < playerList.size(); j+=2) { 
					if (j != i) {
						teammates+= " " + playerList.get(j).name;
					}
				}
				ch.get(i).out.writeUTF("Your Team: The Pastafarians Plus One Angry Westboro Baptist\nTeammates: " + teammates + "\n\n");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// Pass the socket list to the Controller, so it can handle communication related to game
	Controller cntr = new Controller(playerList, ch);
	// For now, play full game. Ability to play shorter games may come later
	cntr.playGame();
	
    // Close all sockets
	for (ConnectionHandler handle : ch) {
		try {
			handle.out.writeUTF("Goodbye!");
			handle.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
   }
	

   public static void main(String [] args) {
      int port = Integer.parseInt(args[0]);
      while (true) {
      try
      {
    	 Server s;
    	 if (args[0] == null)
    		 s = new Server();
    	 else 
    		 s = new Server(port);
         s.run();
      } catch(IOException e) {
         e.printStackTrace();
      } catch (Exception e) {
    	  e.printStackTrace();
      }
      }
   }
}