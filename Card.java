// Object representing a single Card

public class Card {
	
	/* For simplicity's sake, we are using 14 for Ace, so comparing becomes easier */
	public static final String SPADES = "S";
	public static final String HEARTS = "H";
	public static final String CLUBS = "C";
	public static final String DIAMONDS = "D";
	public static final String JOKER = "J";
	public static final int ACE = 14; 
	public static final int TWO = 2;
	public static final int THREE = 3;
	public static final int FOUR = 4;
	public static final int FIVE = 5;
	public static final int SIX = 6;
	public static final int SEVEN = 7;
	public static final int EIGHT = 8;
	public static final int NINE = 9;
	public static final int TEN = 10;
	public static final int JACK = 11;
	public static final int QUEEN = 12;
	public static final int KING = 13;
	public static final int SMALL = 0;
	public static final int LARGE = 1;
	   
	private String suit;
	private int number;
	
	public Card (int n, String suit) {
		this.suit = suit;
		number = n;
	}

	/**
	 * This will parse the first valid card name it finds.
	 */
	public static Card parseCard(String x) {
		x = x.trim();
		
		String n = x.substring(0,1);
		if (n.equalsIgnoreCase("A")) 
			return new Card(ACE, x.substring(1,2));
		else if (n.equalsIgnoreCase("J"))
			return new Card(JACK, x.substring(1,2));
		else if (n.equalsIgnoreCase("Q"))
			return new Card(QUEEN, x.substring(1,2));
		else if (n.equalsIgnoreCase("K"))
			return new Card(KING, x.substring(1,2));
		else if (n.equalsIgnoreCase("s"))
			return new Card(SMALL, x.substring(1,2));
		else if (n.equalsIgnoreCase("L"))
			return new Card(LARGE, x.substring(1,2));
		else if (x.substring(0,2).equals("10"))
			return new Card(TEN, x.substring(2,3));
		else 
			return new Card(Integer.parseInt(n), x.substring(1,2)); 
	}
		
	public String getSuit() {
		return suit;
	}
	
	public int getNumber() {
		return number;
	}
	
	public boolean equals(Card c) {
		if (c.getNumber() == number && c.getSuit().equals(suit))
			return true;
		else
			return false;
	}
		
	public String toString() {
		String s = "";
		if (number == ACE) 
			s += "A";
		else if (number == JACK)
			s += "J";
		else if (number == QUEEN)
			s += "Q";
		else if (number == KING)
			s += "K";
		else if (number == SMALL)
			s += "s";
		else if (number == LARGE)
			s += "L";
		else 
			s += String.valueOf(number);
		return s + suit;
	}
}