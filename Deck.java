import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Collections;
import java.util.Random;

public class Deck {
	
	private ArrayList<Integer> deck;
	public static Hashtable<Integer,Card> th;

	static {
		th = new Hashtable<Integer,Card>(73);
		th.put(1, new Card(Card.ACE, Card.SPADES));
		th.put(2, new Card(Card.TWO, Card.SPADES));
		th.put(3, new Card(Card.THREE, Card.SPADES));
		th.put(4, new Card(Card.FOUR, Card.SPADES));
		th.put(5, new Card(Card.FIVE, Card.SPADES));
		th.put(6, new Card(Card.SIX, Card.SPADES));
		th.put(7, new Card(Card.SEVEN, Card.SPADES));
		th.put(8, new Card(Card.EIGHT, Card.SPADES));
		th.put(9, new Card(Card.NINE, Card.SPADES));
		th.put(10, new Card(Card.TEN, Card.SPADES));		
		th.put(11, new Card(Card.JACK, Card.SPADES));
		th.put(12, new Card(Card.QUEEN, Card.SPADES));
		th.put(13, new Card(Card.KING, Card.SPADES));
		th.put(14, new Card(Card.ACE, Card.HEARTS));
		th.put(15, new Card(Card.TWO, Card.HEARTS));
		th.put(16, new Card(Card.THREE, Card.HEARTS));
		th.put(17, new Card(Card.FOUR, Card.HEARTS));
		th.put(18, new Card(Card.FIVE, Card.HEARTS));
		th.put(19, new Card(Card.SIX, Card.HEARTS));
		th.put(20, new Card(Card.SEVEN, Card.HEARTS));
		th.put(21, new Card(Card.EIGHT, Card.HEARTS));
		th.put(22, new Card(Card.NINE, Card.HEARTS));
		th.put(23, new Card(Card.TEN, Card.HEARTS));		
		th.put(24, new Card(Card.JACK, Card.HEARTS));
		th.put(25, new Card(Card.QUEEN, Card.HEARTS));
		th.put(26, new Card(Card.KING, Card.HEARTS));
		th.put(27, new Card(Card.ACE, Card.CLUBS));
		th.put(28, new Card(Card.TWO, Card.CLUBS));
		th.put(29, new Card(Card.THREE, Card.CLUBS));
		th.put(30, new Card(Card.FOUR, Card.CLUBS));
		th.put(31, new Card(Card.FIVE, Card.CLUBS));
		th.put(32, new Card(Card.SIX, Card.CLUBS));
		th.put(33, new Card(Card.SEVEN, Card.CLUBS));
		th.put(34, new Card(Card.EIGHT, Card.CLUBS));
		th.put(35, new Card(Card.NINE, Card.CLUBS));
		th.put(36, new Card(Card.TEN, Card.CLUBS));		
		th.put(37, new Card(Card.JACK, Card.CLUBS));
		th.put(38, new Card(Card.QUEEN, Card.CLUBS));
		th.put(39, new Card(Card.KING, Card.CLUBS));
		th.put(40, new Card(Card.ACE, Card.DIAMONDS));
		th.put(41, new Card(Card.TWO, Card.DIAMONDS));
		th.put(42, new Card(Card.THREE, Card.DIAMONDS));
		th.put(43, new Card(Card.FOUR, Card.DIAMONDS));
		th.put(44, new Card(Card.FIVE, Card.DIAMONDS));
		th.put(45, new Card(Card.SIX, Card.DIAMONDS));
		th.put(46, new Card(Card.SEVEN, Card.DIAMONDS));
		th.put(47, new Card(Card.EIGHT, Card.DIAMONDS));
		th.put(48, new Card(Card.NINE, Card.DIAMONDS));
		th.put(49, new Card(Card.TEN, Card.DIAMONDS));		
		th.put(50, new Card(Card.JACK, Card.DIAMONDS));
		th.put(51, new Card(Card.QUEEN, Card.DIAMONDS));
		th.put(52, new Card(Card.KING, Card.DIAMONDS));
		th.put(53, new Card(Card.LARGE, Card.JOKER));
		th.put(54, new Card(Card.SMALL, Card.JOKER));
	}
	
	public Deck(int numDecks) {
		deck = new ArrayList<Integer>(numDecks*54);
		for (int i = 0; i < numDecks; i++) {
			for (int j = 1; j <= 54; j++) {
				deck.add(j);
			}
		}
		long seed = System.nanoTime();
		Collections.shuffle(deck, new Random(seed));
	}
	
	/* Gets top card of deck, then removes it */
	public Card draw() {
		Integer top = deck.get(0);
		deck.remove(0);
		return th.get(top);
	}
	
	public int cardsLeft() {
		return deck.size();
	}
	
}